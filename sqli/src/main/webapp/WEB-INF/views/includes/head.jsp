<%@ include file="/WEB-INF/views/includes/references.jsp"%>
    <head>
        <meta charset="UTF-8">
        <title>Login | TechDiggity</title>
        <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/bootstrap.css">
        <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/style.css">
        <script src="<%=request.getContextPath() %>/resources/js/jquery.min.js"></script>
        <script src="<%=request.getContextPath() %>/resources/js/bootstrap.js"></script>
        <script src="<%=request.getContextPath() %>/resources/js/jquery.dataTables.min.js"></script>
        <script src="<%=request.getContextPath() %>/resources/js/dataTables.bootstrap.min.js"></script>	
        <script type="text/javascript">
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>